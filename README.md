# Generic multipart Upload Server

## How to build the container ?

```bash
git clone https://gitlab.com/broadcast-tools/upload-server.git
cd upload-server
docker build --pull . -t upload-server
docker run -it --rm -p "3000:3000" -v /data:/data -e UPLOAD_PATH=/data upload-server
```
